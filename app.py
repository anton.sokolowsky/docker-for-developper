"""
Simple example to run a python app in docker
"""

import os
import socket

from flask import Flask
from redis import Redis, RedisError
from importlib.metadata import version

# Connect to Redis
redis = Redis(host="redis_db", db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    """
    Simple hello world
    """
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>Cannot connect to Redis, counter disabled</i>"

    flask_version = version('flask')
    name=os.getenv("NAME", "world")
    hostname=socket.gethostname()
    html = f"""<h3>Hello {name}!</h3>
           <b>Hostname:</b> {
                   hostname.upper() # Use upper case, this comment will cause a syntax error with python < 3.12
                   }<br/>
           <b>Visits:</b> {visits}<br/>
           Powered by Flask {flask_version}"""

    return html

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8080)
